/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		'./components/**/*.{js,vue,ts}',
		'./layouts/**/*.vue',
		'./pages/**/*.vue',
		'./plugins/**/*.{js,ts}',
		'./nuxt.config.{js,ts}'
	],
	theme: {
		extend: {
			colors: {
				black: '#252525',
				orange: '#ff6600',
				red: '#dd2127',
				green: '#30981d',
				grey: '#6e6e6e',
				darkGrey: '#565656',
				blue: '#4f8ad0',
				bodyText: '#3d3d3d',
				primary: {
					100: '#CEF3E9',
					200: '#578c7e',
					300: '#3f7b6b',
					400: '#276b59',
					500: '#0f5a46',
					600: '#0e513f',
					700: '#0c4838',
					800: '#0b3f31',
					900: '#09362a'
				},
				yellow: {
					100: '#FFE599',
					200: '#ffd24d',
					300: '#ffcc33',
					400: '#ffc519',
					500: '#ffbf00',
					600: '#e6ac00',
					700: '#cc9900',
					800: '#b38600',
					900: '#997300'
				}
			}
		}
	},
	plugins: []
}
