import Vue from 'vue'
import * as VueGoogleMaps from '~/node_modules/vue2-google-maps/src/main.js'

Vue.use(VueGoogleMaps, {
	load: {
		key: 'AIzaSyDnMuxcCe0PAMytcPWZWtNxF4hA-gcLe68',
		libraries: 'places'
	}
})
